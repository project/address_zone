<?php

/**
 * @file
 * Administrative callbacks for the Address Zone module.
 */

/**
 * Form constructor for module admin overview.
 */
function address_zone_admin_overview_form($form, &$form_state) {
  $form = [];

  $header = [
    [
      'data' => t('Zone'),
      'field' => 'zone_name',
      'sort' => 'asc',
    ],
    [
      'data' => t('Description'),
      'field' => 'zone_description',
    ],
    [
      'data' => t('Group'),
      'field' => 'zone_group',
    ],
    [
      'data' => t('Operations'),
    ],
  ];

  $form['zones'] = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => [],
    '#empty' => t('No zones have been created yet.'),
  ];

  // Get available zones.
  $available_zones = address_zone_fetch_zones();

  // Get the current sort and order parameters from the url.
  $tablesort_order = tablesort_get_order($header);
  $tablesort_sort = tablesort_get_sort($header);

  // Sort the table data.
  $sorted_zones = address_zone_sort_zones($available_zones, $tablesort_order['sql'], $tablesort_sort);

  // Process the data.
  $rows =  [];
  foreach ($sorted_zones as $zone) {
    // Make a id <=> group_name key.
    $row_key = address_zone_get_zone_id($zone);

    // Add common values.
    $rows[$row_key]['zone_name'] = $zone['name'];
    $rows[$row_key]['zone_description'] = $zone['description'];
    $rows[$row_key]['zone_group'] = $zone['group_name'];

    $op_links = [];
    $op_links[] = l(t('view file'), file_create_url($zone['uri']));
    // @todo Adds support to remove uploaded files.
    // if (strpos($zone['uri'], ADDRESS_ZONE_PATH) === 0) {
    //   $op_links[] = l(t('remove file'), '');
    // }
    $rows[$row_key]['zone_operations'] = implode(' | ', $op_links);
  }

  $form['zones']['#rows'] = $rows;

  $form['file'] = [
    '#type' => 'file',
    '#title' => t('Upload zone'),
    '#description' => t('Upload a new Zone file, allowed extensions: json.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Form validate handler for module admin overview form.
 */
function address_zone_admin_overview_form_validate($form, &$form_state) {
  $file = file_save_upload('file', [
    // Validate extensions.
    'file_validate_extensions' => ['json'],
  ]);

  // If the file do not passed validation:
  if (!$file) {
    form_set_error('file', t('No file was uploaded.'));
    return;
  }
  // Verify if the uploaded file is a valid zone.
  $zone = drupal_json_decode(file_get_contents($file->uri));
  if (empty($zone['id']) || empty($zone['name']) || empty($zone['description']) ||
    empty($zone['countries']) || count($zone['countries']) == 0) {
    form_set_error('file', t('The uploaded file does not appears a valid zone.'));
    return;
  }
  // Move the file into the defined module upload path.
  if ($file = file_move($file, ADDRESS_ZONE_PATH, FILE_EXISTS_REPLACE)) {
    // Save the file for use in the submit handler.
    $form_state['storage']['file'] = $file;
  }
  else {
    form_set_error('file', t("Failed to write the uploaded file to the site's file folder."));
  }
}

/**
 * Form submit handler for module admin overview form.
 */
function address_zone_admin_overview_form_submit($form, &$form_state) {
  $file = $form_state['storage']['file'];

  // Remove it from storage.
  unset($form_state['storage']['file']);

  // Make the storage of the file permanent.
  $file->status = FILE_STATUS_PERMANENT;

  // Save file status.
  file_save($file);

  // Reset zones and display a message.
  address_zone_reset_zones();
  drupal_set_message(t('New zone has been submitted and saved, filename: @filename.', array('@filename' => $file->filename)));
}

/**
 * Function sorting callback, sorts zones by field name.
 *
 * @param array $data
 *   The rows to be sorted.
 * @param string $field_name
 *    The name of the field to sort on.
 * @param string $sort
 *   The current sort direction ("asc" or "desc").
 *
 * @return array
 *   The sorted rows.
 */
function address_zone_sort_zones($data, $field_name, $sort) {
  switch ($field_name) {
    case 'zone_description':
      uasort($data, 'address_zone_sort_description');
      return ($sort == 'desc') ? array_reverse($data, TRUE) : $data;

    case 'zone_group':
      uasort($data, 'address_zone_sort_name');
      uasort($data, 'address_zone_sort_group');
      return ($sort == 'desc') ? array_reverse($data, TRUE) : $data;

    case 'zone_name':
      uasort($data, 'address_zone_sort_name');
      return ($sort == 'desc') ? array_reverse($data, TRUE) : $data;

    default:
      return $data;
  }
}

/**
 * Array sorting callback; sorts elements by 'description' key.
 *
 * @param string $description_a
 *   First item for comparison.
 * @param string $description_b
 *   Second item for comparison.
 *
 * @return integer
 *   An integer less than, equal to, or greater than zero if the first argument
 *   is considered to be respectively less than, equal to, or greater than the
 *   second.
 */
function address_zone_sort_description($description_a, $description_b) {
  if (!isset($description_b['description'])) {
    return -1;
  }
  if (!isset($description_a['description'])) {
    return 1;
  }

  return strcasecmp($description_a['description'], $description_b['description']);
}

/**
 * Array sorting callback; sorts elements by 'group_name' key.
 *
 * @param string $group_a
 *   First item for comparison.
 * @param string $group_b
 *   Second item for comparison.
 *
 * @return integer
 *   An integer less than, equal to, or greater than zero if the first argument
 *   is considered to be respectively less than, equal to, or greater than the
 *   second.
 */
function address_zone_sort_group($group_a, $group_b) {
  if (!isset($group_b['group_name'])) {
    return -1;
  }
  if (!isset($group_a['group_name'])) {
    return 1;
  }

  return strcasecmp($group_a['group_name'], $group_b['group_name']);
}

/**
 * Array sorting callback; sorts elements by 'name' key.
 *
 * @param string $name_a
 *   First item for comparison.
 * @param string $name_b
 *   Second item for comparison.
 *
 * @return integer
 *   An integer less than, equal to, or greater than zero if the first argument
 *   is considered to be respectively less than, equal to, or greater than the
 *   second.
 */
function address_zone_sort_name($name_a, $name_b) {
  if (!isset($name_b['name'])) {
    return -1;
  }
  if (!isset($name_a['name'])) {
    return 1;
  }

  return strcasecmp($name_a['name'], $name_b['name']);
}

/**
 * Form constructor for module admin settings.
 */
function address_zone_admin_settings_form($form, &$form_state) {
  $form = [];

  $form['address_zone_path'] = [
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#default_value' => variable_get('address_zone_paths', ''),
    '#description' => t('Enter one path per line.'),
  ];

  $form['#submit'][] = 'address_zone_admin_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Form submit handler for module admin settings form.
 */
function address_zone_admin_settings_form_submit($form, &$form_state) {
  address_zone_reset_zones();
}
