<?php

/**
 * @file
 * Provides Inline Conditions integration for the Address Zone module.
 */

/**
 * Implements hook_inline_conditions_info().
 */
function commerce_address_zone_inline_conditions_info() {
  return [
    'commerce_address_zone_order_address_in_zone' => [
      'label' => t('Order address belong to zone'),
      'entity type' => 'commerce_order',
      'callbacks' => [
        'configure' => 'commerce_address_zone_order_address_in_zone_configure',
        'build' => 'commerce_address_zone_order_address_in_zone_build',
      ],
    ]
  ];
}

/**
 * Configuration callback for commerce_addres_zone_order_address_belong_zone.
 *
 * @param array $settings
 *   An array of rules condition settings.
 *
 * @return array;
 *   A form element array.
 */
function commerce_address_zone_order_address_in_zone_configure($settings) {
  $form = [];

  module_load_include('inc', 'address_zone', 'address_zone.rules');
  module_load_include('inc', 'commerce_order', 'commerce_order.rules');

  // Get options lists.
  $address_fields = commerce_order_address_field_options_list();
  $zone_ids = address_zone_zones_options_list();

  // Ensure we have default values for the condition settings.
  $settings += [
    'address_field' => reset(key($address_fields)),
    'zone' => reset(key($zone_ids)),
  ];

  $form['address_field'] = [
    '#type' => 'select',
    '#title' => t('Address'),
    '#options' => $address_fields,
    '#description' => t('The address associated with this order whose component you want to compare.'),
    '#default_value' => $settings['address_field'],
  ];

  $form['zone'] = [
    '#type' => 'select',
    '#title' => t('Zone'),
    '#options' => $zone_ids,
    '#description' => t('The zone to compare against the address component.'),
    '#default_value' => $settings['zone'],
  ];

  return $form;
}
