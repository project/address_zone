<?php

/**
 * @file
 * Rules integration for the Address Zone module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function address_zone_rules_condition_info() {
  return [
    'address_zone_address_in_zone' => [
      'label' => t('Address field belongs to zone'),
      'parameter' => [
        'address_field' => [
          'type' => 'addressfield',
          'label' => t('Address field'),
          'description' => t('The address field that you want to compare.'),
        ],
        'zone' => [
          'type' => 'text',
          'label' => t('Zone'),
          'options list' => 'address_zone_zones_options_list',
          'description' => t('The zone to compare against the address component.'),
          'restriction' => 'input',
        ],
      ],
      'group' => t('Address Zone'),
      'callbacks' => [
        'execute' => 'address_zone_address_in_zone',
      ],
    ],
  ];
}

/**
 * Condition callback: Determines if an address component belong to the given
 * zone.
 */
function address_zone_address_in_zone($address_field, $zone_id) {
  // If we actually received a valid address field...
  if (!empty($address_field)) {
    // Return the match result.
    return address_zone_match_zone($address_field, $zone_id);
  }

  return FALSE;
}

/**
 * Options list callback: Zones for the address comparison condition.
 */
function address_zone_zones_options_list() {
  $zones = [];

  // Define an options list based in the zone group name.
  foreach (address_zone_fetch_zones() as $id => $zone) {
    $zones[$zone['group_name']][$id] = $zone['name'];
  }

  return $zones;
}
