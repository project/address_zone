<?php

/**
 * @file
 * Rules integration for the Commerce Address Zone module.
 */

// Load address_zone.rules.inc from the Address zone module.
module_load_include('inc', 'address_zone', 'address_zone.rules');
// Load address_zone.rules.inc from the Address zone module.
module_load_include('inc', 'commerce_order', 'commerce_order.rules');


/**
 * Implements hook_rules_condition_info().
 */
function commerce_address_zone_rules_condition_info() {
  $inline_conditions = inline_conditions_get_info();

  return [
    'commerce_address_zone_order_address_in_zone' => [
      'label' => t('Order address belongs to zone'),
      'parameter' => [
        'commerce_order' => [
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The order containing the profile reference with the address in question.'),
        ],
        'address_field' => [
          'type' => 'text',
          'label' => t('Address'),
          'options list' => 'commerce_order_address_field_options_list',
          'description' => t('The address associated with this order whose component you want to compare.'),
          'restriction' => 'input',
        ],
        'zone' => [
          'type' => 'text',
          'label' => t('Zone'),
          'options list' => 'address_zone_zones_options_list',
          'description' => t('The zone to compare against the address component.'),
          'restriction' => 'input',
        ],
      ],
      'group' => t('Commerce Order'),
      'callbacks' => [
        'execute' => $inline_conditions['commerce_address_zone_order_address_in_zone']['callbacks']['build'],
      ],
    ],
  ];
}

/**
 * Build callback: Determines if an address component belong to the given zone.
 */
function commerce_address_zone_order_address_in_zone_build($order, $address_field, $zone_id) {
  list($field_name, $address_field_name) = explode('|', $address_field);

  // If we actually received a valid order...
  if (!empty($order)) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    // And if we can actually find the requested address data...
    if (!empty($order_wrapper->{$field_name}) && !empty($order_wrapper->{$field_name}->{$address_field_name})) {
      $address_field_value = $order_wrapper->{$field_name}->{$address_field_name}->value();

      // Return the match result.
      return address_zone_match_zone($address_field_value, $zone_id);
    }
  }

  return FALSE;
}
