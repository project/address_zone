<?php

/**
 * @file
 * Hooks provided by the Address Zone module.
 */

/**
 * Allow modules to modify the final $zones array.
 *
 * @param array $zones
 *   An array of zones with all its information, keyed by zone id.
 *
 * @see address_zone_fetch_zones()
 */
function hook_address_zone_zones_alter(&$zones) {
  // Set my module zone definitions path.
  $my_module_zones = drupal_get_path('module', 'my_module') . '/zones';

  // Add my module zone definitions.
  $config = address_zone_get_config_files($my_module_zones);
  foreach ($config as  $zone_file_uri => $zone) {
    $zones[address_zone_get_zone_id($zone)] = [
      'uri' => $zone_file_uri,
      'id' => $zone['id'],
      'name' => $zone['name'],
      'description' => $zone['description'],
      'group_name' => $zone['group_name'],
      'countries' => $zone['countries'],
    ];
  }
}
