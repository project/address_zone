## Summary
This module integrate the Zone library [1] of CommerceGuys with Drupal, allowing
you to define territorial groupings mostly used for shipping or tax purposes.

For example, a set of shipping rates associated with a zone where the rates
become available only if the customer's address matches the zone.

This module provide rules and inline conditions / discounts integration, you can
heck if a defined address field belong to a defined zone.

[1] https://github.com/commerceguys/zone

## Requirements
 1. PHP >= 5.4
 2. Download and extract the following libraries into sites/all/libraries.
      - Addressing - https://github.com/commerceguys/addressing
      - Zone - https://github.com/commerceguys/zone
      - Collections - https://github.com/doctrine/collections
 3. Drupal modules:
      - Address field - https://drupal.org/project/addressfield
      - Registry autoload - https://drupal.org/project/registry_autoload
      - Rules - https://drupal.org/project/rules

## Installation
 1. Place this module in the folder sites/[all]/modules/contrib.
 2. Go to the Module page at Administer > Modules and enable it.

## Configuration and usage instructions
- Add a new rule.
- Add a condition 'Entity has field' -> your_addresfield_name
- Add other condition 'Address field belong to zone' -> your_zone

## Submodules
This module include a Drupal Commerce implementation submodule.

## Sponsors
 * This Drupal 7 module is maintained and developed by http://cambrico.net/
   Get in touch with us for customizations and consultancy:
   http://cambrico.net/contact
 * Sponsored by http://lanasyovillos.com

## About the authors
 * Pedro Cambra (https://www.drupal.org/u/pcambra)
 * Manuel Egío (https://www.drupal.org/u/facine)
